#!/usr/bin/env python3
# Modellrechnung zum Fangstoß in Abhängigkeit vom Sturzfaktor.
# Michael Reisecker, 2023

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

def fangstoss(ff, MM, SS):
    """Berechnung des Fangstosses anhand der altbekannten einfachen Fangstossformel.

    param ff Sturzfaktor (einheitslos).
	param MM Sturzmasse in Kilogramm.
    param SS Seilmodul in Newton.
    """
    return MM * gg + np.sqrt(MM*MM * gg*gg  + 2 * MM * gg * SS * ff)

SS = 25e3 # Seilmodul (N)
SS_Hanf = 700e3
SS_Stahl = 1000e3
SS_Dyneema = 500e3
SS_Reepschnur = 235e3 # Polyamid: 3 GPa, 10 mm Seil
SS_Bungee = 0.8e3
MM = 80 # Mase (kg)
MM2 = 60
MM3 = 100
gg = 9.81 # Erdbeschleunigung (ms^-2)

ff = np.arange(0.0, 2.02, 0.02) # Fangstoss

# Fangstosskurven:
fig = plt.figure()
plt.plot(ff, fangstoss(ff, MM, SS_Stahl), 'r', label='Stahlseil')
plt.plot(ff, fangstoss(ff, MM, SS_Hanf), 'g', label='Hanfseil')
plt.plot(ff, fangstoss(ff, MM, SS_Dyneema), 'b', label='Dyneema')
plt.plot(ff, fangstoss(ff, MM, SS_Reepschnur), 'c', label='Reepschnur')
plt.plot(ff, fangstoss(ff, MM, SS), 'k', label='Kletterseil')
plt.plot(ff, fangstoss(ff, MM2, SS), 'k--', label='_nolegend')
plt.plot(ff, fangstoss(ff, MM3, SS), 'k:', label='_nolegend')
plt.plot(ff, fangstoss(ff, MM, SS_Bungee), 'm', label='Bungee-Seil')

# Metadaten:
plt.title('Fangstoß in Abhängigkeit vom Sturzfaktor')
plt.ylabel('Fangstoß (kN)')
plt.xlabel('Sturzfaktor')
plt.legend(framealpha=1)
plt.grid()

# Fangstoss in kN anstatt N zeigen:
y_scale = 1/1000
ax = plt.gca()
ax.set_ylim(ymin=0)
ax.set_xlim(xmin=0, xmax=ff[-1])
y_ticks = ticker.FuncFormatter(lambda yy, pos: '{0:g}'.format(yy * y_scale))
ax.yaxis.set_major_formatter(y_ticks)

# Unterschiedliche Fallmassen in rechte Achse zeichnen:
ax_r = ax.twinx()
max_M = fangstoss(ff[-1], MM, SS)
max_M2 = fangstoss(ff[-1], MM2, SS)
max_M3 = fangstoss(ff[-1], MM3, SS)
offset = 1.5e3
r_ticks = [max_M2 - offset, max_M, max_M3 + offset]
ax_r.set_yticks(r_ticks)
ax_r.set_yticklabels([f'm={MM2} kg', f'm={MM} kg', f'm={MM3} kg'])
ax_r.set_ylim(ax.get_ylim())
ax_r.tick_params(right = False)

fig.tight_layout()
plt.savefig('fangstoss.pdf')
