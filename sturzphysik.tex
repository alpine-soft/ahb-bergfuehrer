\documentclass[10pt]{article}

\usepackage[table]{xcolor}
\usepackage{footnote}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{lastpage}
\usepackage{footnote}
\usepackage{colortbl}
\usepackage{xcolor}
\usepackage{xspace} 
\usepackage{amsmath}
\usepackage[ngerman]{babel}
\usepackage[most]{tcolorbox}
\usepackage{fancyhdr}
\usepackage{units}
\usepackage{bibgerm}
\usepackage[textfont=it,hypcap=false]{caption}
\usepackage{setspace}
\usepackage[no-math]{fontspec} % font style for text by fontspec
\usepackage{icomma}

\usepackage{mathspec} % font style in formulas by mathspec
\makeatletter % mathspec makes some bad changes - undo them
\let\RequirePackage\original@RequirePackage
\let\usepackage\RequirePackage
\makeatother

\usepackage{mathtools}
\usepackage{mathastext}
\usepackage{geometry}
\geometry{
    a4paper,
    total={180mm, 257mm},
    left=15mm,
    top=15mm,
}

\pagestyle{fancy}
\definecolor{textgray}{gray}{0.1}
\definecolor{lightgray}{gray}{0.8}
\definecolor{boxgray}{gray}{0.5}
\setmainfont{Myriad Pro Condensed}
\setmathsfont(Digits){Myriad Pro Condensed}

\setstretch{1.2} %12/10
\setlength{\parindent}{3mm}
\setlength{\textwidth}{180mm}

\addto\captionsngerman{\renewcommand{\figurename}{\emph{Abb.}}}

\lhead{}
\chead{}
\rhead{\textcolor{lightgray}{Sturzphysik, Mag. Michael Reisecker}}
\cfoot{}
\rfoot{\color{black} Seite \thepage /\pageref*{LastPage}} %Farbe fuer Ausrichtung
\renewcommand{\headrulewidth}{0pt}

\newcommand{\mytilde}{{\raise.17ex\hbox{$\scriptstyle\mathtt{\sim}$}}\xspace} 
\newcommand\VRule{\color{lightgray}\vrule width 0.5pt}
\newcolumntype{L}{>{\raggedleft}p{0.14\textwidth}}
\newcolumntype{R}{p{0.8\textwidth}}

%\definecolor{darkblue}{rgb}{0,0,0.5}
%\hypersetup{colorlinks, urlcolor=darkblue}
\hypersetup{
    hidelinks,
    pdfauthor={Mag. Michael Reisecker}
    pdftitle={Sturzphysik im Bergsport},
    pdfsubject={Sturzphysik},
    pdfkeywords={Fangstoß, Fangstoss, Sturzfaktor, Elastizitätsmodul, Seildehnung}
}
\urlstyle{same}

\DeclareTColorBox[auto counter]{physicsbox}{ O{} m }{
colframe=boxgray,
coltitle=textgray,
sharp corners,
enhanced,
colback=white,
%breakable,
before upper={\parindent 3mm},
boxed title style={
    empty,
    boxrule=1pt,
    sharp corners,
    interior code={
        \fill [white] (interior.north west) rectangle (interior.south east);
        \filldraw [boxgray] (interior.north west) rectangle ([xshift=15cm]interior.south west);
    },
},
attach boxed title to top,
title = \large\textbf{\textcolor{white}{#2}}\normalsize \hfill Physikkasten \thetcbcounter
}

\DeclareTColorBox[auto counter]{calcbox}{ O{} m }{
colframe=boxgray,
coltitle=textgray,
sharp corners,
enhanced,
colback=white,
%breakable,
%break at=15cm,
before upper={\parindent 3mm},
boxed title style={
    empty,
    boxrule=1pt,
    sharp corners,
    interior code={
        \fill [white] (interior.north west) rectangle (interior.south east);
        \filldraw [boxgray] (interior.north west) rectangle ([xshift=15cm]interior.south west);
    },
},
attach boxed title to top,
title = \large\textbf{\textcolor{white}{#2}}\normalsize \hfill Beispielrechnung \thetcbcounter
}

\begin{document}

\color{textgray}

\tableofcontents

\section{Sturzphysik im Bergsport}

\subsection{Kräfte beim Klettern}

Zur Veranschaulichung der auftretenden Kräfte bei relevanten Themen des Bergsports verwenden wir vorerst die Gesetzmäßigkeit \emph{Energie ist Kraft mal Weg}. Damit lässt sich bereits die Größenordnung der relevanten Größen schätzen (Beispielrechnung 1).

\input{boxes/calc-sturzenergie}

Die Höhe, die der Kletterer über die letzte Zwischensicherung steigt wird als \emph{Sturzpotenzial} bezeichnet. Die \emph{freie Sturzhöhe} ist das Doppelte davon (ohne Schlappseil), also die Länge die der Stürzende fällt, bis das Seil zu spannen beginnt. Addiert man den darauf folgenden Bremsweg zur freien Sturzhöhe so erhält man die \emph{gesamte Sturzhöhe}.
\subsection{Fangstoß und Sturzfaktor}

Der Krafteintrag bei einem Sturz wird unter Kletterern als \textbf{Fangstoß} bezeichnet. Der Fangstoß ist also ein Maß für den Ruck, den ein Kletterer fühlt und ist kennzeichnend für die Härte des Sturzes. Je weiter ein Kletterer fällt, desto größer ist die Energie die bis zum Stillstand abgebaut werden muss (siehe Physikkasten 1). Damit die auftretenden Kräfte überlebt werden können muss das Seil elastisch sein; je elastischer das Seil ist, desto geringer ist der Fangstoß. Glücklicherweise erhöht sich bei größeren Sturzhöhen die Dehnung des Seiles; verdoppelt sich die Länge des freien Seiles so verdoppelt sich auch die Dehnung. Auf den ersten Blick etwas überraschend ist die Sturzhöhe also nicht ausschlaggebend, sondern eben dieses Verhältnis aus Sturzhöhe zu ausgegebener Seillänge, genannt der \textbf{Sturzfaktor} (siehe Physikkasten 2).

Er liegt zwischen 0 (Toprope) und 2 (Sturz in den Stand). Höhere Sturzfaktoren können nur in Ausnahmefällen auftreten, z. B. am Klettersteig, wenn der Sichernde Seil einholt oder wenn viel Reibung im Spiel ist, wenn das Seil z. B. an einer Dachkante abgeklemmt wird.

Fällt ein Kletterer, so zieht seine Gewichtskraft am Seil. Wenn dieses über einen Umlenkpunkt läuft, so erfährt der Sicherer fast die gleiche Kraft, den \textbf{Sturzzug}. Nachdem an der Umlenkung \textbf{Reibung} überwunden werden muss, muss der Kletterer stärker ziehen damit eine bestimmte Kraft am Sicherer ankommt. Auf Seite des Kletterers sind die Kräfte dadurch um 50 bis \unit[70]{\%} höher.
\smallskip

\input{boxes/calc-kraft}

Die Formel für den Fangstoß lautet (siehe Physikkasten 2):
\begin{equation*}
F_\text{max} = mg + \sqrt{m^2g^2 + 2mgS \cdot f}
\end{equation*}

Dabei ist $m$ die Masse des Kletterers, $g$ die Erdbeschleunigung (ca. $\unit[10]{\text{m}/\text{s}^2}$), $S$ der Seilmodul (Elastizität des Seiles), und $f$ der wichtige Sturzfaktor. Der Fangstoß wird für einige Seiltypen in Abb. \ref{fig:fangstoss}  gezeigt.

\begin{center}
\includegraphics[width=0.7\textwidth]{images/fangstoss} 
\captionof{figure}{Der Fangstoß für einige Seiltypen. Beim dynamischen Kletterseil (schwarze Kurve) sind verschiedene Fallmassen gerechnet.}
\label{fig:fangstoss}
\end{center}

\input{boxes/calc-seilmodul}

Typische Fangstöße beim Sportklettern liegen bei ca. \unit[2 - 2,5]{kN} \cite{magd:belastung}\cite{ry:forces}, mit Reibung an den Zwischensicherungen etwas darüber. Nach oben hin sind die Werte bei großen Alpinkletter-Stürzen durch die Norm auf \unit[12]{kN} begrenzt, typischerweise je nach Seil zwischen 7,4 und \unit[9,5]{kN} bei Einfachseilen und \unit[5,5 - 6,5]{kN} bei Halbseilen. Moderate Stürze liegen dementsprechend irgendwo dazwischen.
\smallskip

\input{boxes/calc-fangstoss}

\subsection{Sonderfälle}

\subsubsection{Fangstoß im Toprope}

Die Fallhöhe ist hierbei $0$. Setzt man dies in Gleichung (\ref{eq:fangstoss}) ein so erhält man:
\begin{equation*}
F_\text{max}=2mg
\end{equation*}

Der Sicherer muss also das doppelte Körpergewicht halten und auf die Umlenkung wirkt das Vierfache. Der Grund für den Faktor 2 im Vergleich zur statischen Kraft ist die Trägheitskraft am Umkehrpunkt des schwingenden Seiles.

\subsubsection{Fangstoß in Quergängen}

Der Fangstoß der in Quergängen auftritt kann nicht berechnet werden ohne die komplizierten Bewegungs-Differenzialgleichungen eines elastischen Pendels zu lösen. Für unseren Fall genügt es zwei Extremfälle zu betrachten, nämlich den eines sehr weichen Seiles (durch die hohe Dehnung verläuft der Sturz anfangs vertikal) und den eines sehr harten Seiles (der Körper wird auf eine Kreisbahn gezwungen). Es ergibt sich durch diese Limits \cite{leu:fangstoss}:

\begin{equation*}
2mg \le F_\text{max} \le 3mg
\end{equation*}

Das heißt der Fangstoß bewegt sich unabhängig der Länge des Pendlers zwischen dem 2fachen und 3fachen Körpergewicht und ist an sich nicht gefährlich. Wie jeder Kletterer weiß birgt ein Pendelsturz jedoch die Gefahr mit hoher Rotationsenergie am Fels aufzuschlagen oder daran entlang zu schrammen. Die vertikale Geschwindigkeit mit der man gegen den Fels stößt ist dabei die gleiche, die man im freien Fall erreichen würde!

Stürzt man in kleinen Winkeln zur Senkrechten so ist nach wie vor die Sturzhöhe ausschlaggebend für die auftretenden Kräfte.

\subsubsection{Fangstoß mit Schlappseil}

Ist Schlappseil im System so muss dieses in den Gleichungen zur Sturzhöhe und zur Länge des ausgegebenen Seiles addiert werden. Dabei wird einsichtig, dass zwei Fälle auftreten können \cite{leu:fangstoss}: Für einen Sturzfaktor kleiner 1 wird der Fangstoß vergrößert, und bei einem Sturzfaktor größer 1 verkleinert. Das bedeutet, dass im Klettergarten durch Schlappseil der Sturzfaktor immer vergrößert wird. In alpinen Touren kann der Sturzfaktor durch Schlappseil verringert werden, jedoch hilft das nur, wenn der Sturz nicht plötzlich durch ein Band gebremst wird.

\subsection{Reibung im System}

Wie bereits angedeutet kann äußere Reibung, also jene am Fels und an Umlenkpunkten, den Fangstoß erheblich vergrößern. An der Physik des Fangstoßes ändert sich nicht viel, nur dass ein effektiver Sturzfaktor auftritt, der nicht mehr von der Länge des ausgegebenen Seils abhängt, sondern wie viel davon tatsächlich frei wirken kann -- die effektive ausgegebene Seillänge (siehe Physikkasten 4). Bei verschwindend kleiner Reibung folgt daraus wieder die bekannte Formel für den Sturzfaktor. Bei sehr hoher Reibung geht die freie Seillänge Richtung Sturzpotential und der Sturzfaktor ist 2 (als wenn das Seil am Umlenkpunkt fixiert wäre). Dabei ist jedoch noch keine Reibung am Fels miteinbezogen; in solchen Extremfällen können Sturzfaktoren größer als 2 auftreten.

Wie sich Reibung auf den Seilzug auswirkt wird im Physikkasten 5 diskutiert.

\subsection{Dynamische Sicherung}

Bei den bisherigen Überlegungen war das Seil am Sicherungspunkt fixiert. Was ändert sich nun am Fangstoß wenn dynamisch gesichert wird? Betrachten wir zuerst dynamische Körpersicherung mit einem statischen Sicherungsgerät, das heißt der Sichernde kann angehoben werden oder springt gar in den Sturz. Die Rechnung zeigt (siehe Physikkasten 6) dass bei geradem Seilverlauf die auftretenden Kräfte für den Stürzenden auf unter die Hälfte gesenkt werden können, was vor allem bei kontrollierten Sportkletterstürzen von großem Vorteil sein kann. Umgekehrt bedeutet dies auch, dass wenn der Sicherer Seil einholt oder nach hinten läuft gefährlich große Fangstöße auftreten können!

Aber Achtung: diese Technik kann nur im Klettergarten empfohlen werden, wo der Sicherer frei beweglich ist, genug Platz hat, auf den Sturz gefasst ist, nicht überreagiert und die Methode geübt hat. In Mehrseillängentouren, insbesondere beim Alpinklettern, zeigen Versuche und Befragungen von Gestürzten dass sich die Sache ganz anders verhält und sogar härtere Stürze folgen können. Durch den Kontrollverlust wird das Sicherungsgerät nicht mehr sauber bedient und es besteht erhebliche Verletzungsgefahrt durch Anprallen im Fels, was die Sicherung im schlimmsten Fall komplett ausschalten kann. Als Standardtechnik in Mehrseillängenrouten wird also unbedingt die Sicherung vom Standplatz aus empfohlen.

\subsection{Sicherungsgeräte}

Beim Sichern in Mehrseillängenrouten wird man dynamische Seilbremsen verwenden, also z. B. Tuber. Auf Seite des Sichernden ist die Kraft dann durch den Durchlauf im Sicherungsgerät begrenzt (siehe Physikkasten 7). Auf Seite des Kletterers ist die entgegengerichtete Kraft gleich groß und wie oben nur durch die Reibung an der Umlenkung nochmals erhöht.

Abhängig von der Handkraft (ca. \unit[0,4]{kN}) und der verfügbaren Seillänge kann der Sturz in dieser Weise theoretisch so weich gemacht werden wie man möchte mit dem Preis von deutlich längeren Fallhöhen.

\section{Reibungsklettern}

\subsection{Reibungskletterschuhe}

Siehe Physikkasten 9.

\section{Klemmgeräte beim Alpinklettern}

Siehe Physikkästen 10 und 11.

\section{Flaschenzüge im Bergsport}

Die einfachste Form eines Flaschenzugs die uns beim Klettern regelmäßg begegnet ist es, wenn wir uns nach einem Sturz am Seil wieder nach oben ziehen. Nachdem wir so weit ziehen müssen wie wir wieder nach oben wollen verkleinert sich die Kraft nicht und wir müssen unser Körpergewicht heben.

Das gleiche Bild ergibt sich beim Körperflaschenzug, nur dass wir hier mit den Händen mithelfen werden. Anders ist das schon bei der losen Rolle, bei der die Hubarbeit an der Rolle verrichtet wird (deshalb \glqq lose\grqq{} Rolle). Hier müssen wir die doppelte Länge an Seil ziehen, die der Körper bewegt wird da sich die Länge des gezogenen Seils auf beide Seiten der Rolle verteilt. Das heißt dass im Gegenzug die Kraft halbiert wird. Siehe dazu die Abbildungen in Physikkasten 13.

Ohne hier weiter auf die technischen Deteils einzugehen sei erwähnt, dass die bewährten Systeme der behelfsmäßigen Bergrettung versuchen, all diese Umstände mit möglichst einfachen Mitteln umzusetzen und einen guten Mittelweg zu gehen. Sie setzen allesamt gewisse Ideen um und ein fehlerhafter Aufbau kann die Wirksamkeit drastisch verringern. In anderen Aufgabenbereichen wie der professionellen Bergrettung, Highlinen oder beim Bigwall-Klettern/Haulen wird unter Umständen auf andere Systeme zurückgegriffen (siehe z. B. \cite{bus:bigwall} \cite{lutz:seiltechnik}).

\subsection{Flaschenzüge der behelfsmäßigen Bergrettung}

\subsubsection{Expressflaschenzug}

Der Expressflaschenzug (siehe Abbildungen in Physikkasten 12) ist ein guter Kompromiss zur schnellen Hilfe. Die Zugstrecke bleibt überschaubar, der Stand wird nicht sonderlich hoch belastet (\unit[67]{\%} der Gewichtskraft), und er ist vor allem schnell aufgebaut. Der große Nachteil ist der geringe Wirkungsgrad; ist dieser theoretisch schon nur 1:3 so fällt er real auf etwa 1:1.5. Er findet also Anwendung wenn der Gestürzte kurz Hilfe benötigt und gut aktiv mithelfen kann.

\subsubsection{Seilrollenflaschenzug bzw. Schweizer Flaschenzug}

Der Seilrollenflaschenzug ist eine Mischung aus Potenz- und Faktorenflaschenzug (Abb. in Physikkasten 12). Er nutzt eine zusätzliche Reepschnur und verringert somit Dehnung im Seil und Seildurchlauf. Mit relativ wenige Material (plus Rücklaufsperre) wird ein theoretischer Wirkungsgrad von 1:5 erreicht. Real liegt dieser leider eher bei 1:2,5 \cite{math:physik-zeit}. Generell kann gesagt werden dass der einfache Körperflaschenzug oft die besten Chancen hat jemanden nach oben zu befördern. Klappt das nicht, wird es vermutlich auch mit dem Seilrollenflaschenzug schwierig. Jedoch kann er von unschätzbarem Vorteil sein um ein System aufzubauen in welches man nicht eingebunden ist und von wo aus man weitere Maßnahmen einleiten kann.

\subsubsection{Mehrfachflaschenzug}

Man kann die theoretische Übersetzung des Seilrollenflaschenzugs noch auf 1:7 erhöhen, indem man die zusätzliche lose Rolle (die mit der Reepschnur, siehe Abb. in Physikkasten 12) nicht am Stand anschlägt sondern an einem Seilstrang \cite{petzl:spaltenbergung}. Der Materialaufwand bleibt also der gleiche, die semi-statische Reepschnur übernimmt einen Teil der Zugarbeit und es können auch in realen Szenarien sehr hohe Kräfte entwickelt werden. Man zieht effektiv zwei Mal am Seil. Kann ein starkes Einschneiden des Seiles verhindert werden so können mit diesem System unter Umständen auch zwei bewusstlose Personen aus einer Spalte geborgen werden.

\appendix

\section{Physikkästen}

\captionsetup{labelformat=empty}

\input{boxes/physics-sturz}

\input{boxes/physics-sturzfaktor}

\input{boxes/physics-normsturz}

\input{boxes/physics-reibung-umlenkung}

\input{boxes/physics-seilzug}

\input{boxes/physics-koerperdynamik}

\input{boxes/physics-seilbremsen}

\input{boxes/modellgrenzen}

\input{boxes/physics-reibungsklettern}

\input{boxes/physics-klemmkeil}

\input{boxes/physics-friend}

\input{boxes/physics-kraftwandler.tex}

\input{boxes/physics-flaschenzuege}

\newpage
\section {Literatur}
% kein Titel:
\renewcommand{\section}[2]{}%
\bibliography{bibliography}{}
\bibliographystyle{gerplain}

\end{document}
