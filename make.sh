#!/bin/bash

set -e

XELATEX () {
    xelatex sturzphysik.tex
}

BIBTEX () {
    bibtex sturzphysik.aux
}

XELATEX
BIBTEX
XELATEX
XELATEX
